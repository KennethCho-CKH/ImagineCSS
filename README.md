# Imagine CSS
Imagine CSS is a CSS framework that aim to helps you make a modern, responsive, and lightweight website.  
Imagine CSS is currently on development, feel free to go to issue to give me advices!  

![Dog Dancing Meme](https://c.tenor.com/jdcMLq1sfjkAAAAC/dancing-voting-day.gif)

## Install
`git clone https://github.com/KennethCho-CKH/ImagineCSS.git`

## Website
[https://imagine-css.netlify.app/](https://imagine-css.netlify.app/)
